
//packages
import express from 'express';
import fetch from 'node-fetch';
import toSave from '../tools/toSave';
import toUpdate from '../tools/toUpdate';
import toDelete from '../tools/toDelete';
const router = express.Router();
export default router;

router.all('/dev', (req, res) => {
//make your test here
    res.status(200).json(req.body);
});
router.get("/", async (req, res) => {
    try {
        const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/event/get`, { method: 'GET', headers: req.newHeaders });
        const dataCo = await response.text();
        res.status(200).json(dataCo);
    } catch (error) {
        res.status(500).send(error);
    }
});
router.get("/:id", async (req, res) => {
    try {
        const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/element/get/type/events/id/${req.params.id}`, { method: 'GET', headers: req.newHeaders });
        const dataCo = await response.json();
        res.status(200).json(dataCo);
    } catch (error) {
        res.status(500).send(error);
    }
});
/////////save 
///post 
router.post("/", (req, res) => {
    toSave(req, res, "event");
});
//put
//////TODO vérifié valeur vide
router.put("/", async (req, res) => {
    toSave(req, res, "event");
});
//////////delete events
// get and post
router.get("/delete/:id", (req, res) => {
    toDelete(req, res, "events", req.params.id);
})
    .post("/delete/:id", async (req, res) => {
        toDelete(req, res, "events", req.params.id);
    })
    //delete
    .delete('/:id', async (req, res) => {
        toDelete(req, res, "events", req.params.id);
    });

// //////////update project
// router.post('/update', (req, res) => { 
//     toUpdateProject(req, res);
// });


// router.options('/', function (req, res) { res.sendStatus(405); });

//module.exports = router;



