
//packages
import express, { response } from 'express';
import fetch from 'node-fetch';
import toSave from '../tools/toSave';
import toDelete from '../tools/toDelete';


const router = express.Router();
export default router;

router.all('/dev', (req, res) => {
    //make your test here
    res.status(200).json(req.body);
})
    .get("/", async (req, res) => {
        try {
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/project/get`, { method: 'GET', headers: req.newHeaders });
            const dataCo = await response.json();
            res.status(200).json(dataCo);
        } catch (error) {
            res.status(500).send(error);
        }
    })
    .get("/limit/:limit/index/:index", async (req, res) => {
        try {
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/project/get/limit/${req.params.limit}/index/${req.params.index}`,
                { method: 'GET', headers: req.newHeaders });
            const dataCo = await response.json();
            res.status(200).json(dataCo);
        } catch (error) {
            res.status(500).send(error);
        }
    })
    .get("/:id", async (req, res) => {
        try {
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/element/get/type/project/id/${req.params.id}`, { method: 'GET', headers: req.newHeaders });
            const dataCo = await response.json();
            res.status(200).json(dataCo);
        } catch (error) {
            res.status(500).send(error);
        }
    })
    /////////save project
    ///post 
    .post('/', async (req, res) => {
        toSave(req, res, 'project');
    })
    //put
    .put('/', async (req, res) => {
        toSave(req, res, 'project');
    })
    //////////delete project
    //get and post
    .get("/delete/:id", async (req, res) => {
        toDelete(req, res, "projects", req.params.id);
    })
    .post("/delete", async (req, res) => {
        toDelete(req, res, "projects", req.body.id);
    })
    //delete
    .delete('/:id', async (req, res) => {
        toDelete(req, res, "projects", req.params.id);
    })

    //////////update project
    .post('/update', (req, res) => {
        toSave(req, res, "project");
    })
    .patch('/', (req, res) => {
        toSave(req, res, "project");
    })

    .options('/', function (req, res) { res.sendStatus(405); });



