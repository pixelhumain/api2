
//packages
import express from 'express';
import fetch from 'node-fetch';
import FormData from 'form-data';

//tools
import toSave from '../tools/toSave';
import toDelete from '../tools/toDelete';
import updateBuilder from '../tools/updateBuilder';
import makeFormKey from '../tools/formDataBuilder';



const router = express.Router();
export default router;

router.all('/dev', (req, res) => {
    //make your test here
    res.status(200).json(req.body);
});
// router.get("/", async (req, res) => {
//     try {
//         const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/project/get`, { method: 'GET', headers: req.newHeaders });
//         const dataCo = await response.json();
//         res.status(200).json(dataCo);
//     } catch (error) {
//         res.status(500).send(error);
//     }
// });

/////////save links
///post 
router.post('/multiConnect', async (req, res) => {
    try {
        const dataForm = new FormData();
        var toAppend = [];
        for (const [k, v] of Object.entries(req.body)) {
            if (typeof v == 'object' || typeof v == 'array') {
                toAppend = await makeFormKey(k, v, false, toAppend);
            } else {
                dataForm.append(k, v);
            }
        }
        for (var k in toAppend) {
            dataForm.append(k, toAppend[k]);
        }
        const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/link/multiconnect`, { method: 'POST', headers: req.newHeaders, body: dataForm });
        const dataCo = await response.text();
        res.status(200).json(dataCo);
    } catch (error) {
        res.status(500).send(error);
    }
})
    .post('/connect', async (req, res) => {
        try {
            const dataForm = new FormData();
            var toAppend = [];
            for (const [k, v] of Object.entries(req.body)) {
                if (typeof v == 'object' || typeof v == 'array') {
                    toAppend = await makeFormKey(k, v, false, toAppend);
                } else {
                    dataForm.append(k, v);
                }
            }
            for (var k in toAppend) {
                dataForm.append(k, toAppend[k]);
            }
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/link/connect`, { method: 'POST', headers: req.newHeaders, body: dataForm });
            const dataCo = await response.json();
            //const dataCo = await response.json();
            res.status(200).json(dataCo);
        } catch (error) {
            res.status(500).send(error);
        }
    })
    .post('/validate', async (req, res) => {
        try {
            const dataForm = new FormData();
            var toAppend = [];
            for (const [k, v] of Object.entries(req.body)) {
                if (typeof v == 'object' || typeof v == 'array') {
                    toAppend = await makeFormKey(k, v, false, toAppend);
                } else {
                    dataForm.append(k, v);
                }
            }
            for (var k in toAppend) {
                dataForm.append(k, toAppend[k]);
            }
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/link/validate`, { method: 'POST', headers: req.newHeaders, body: dataForm });
            const dataCo = await response.json();
            //const dataCo = await response.json();
            res.status(200).json(dataCo);
        } catch (error) {
            res.status(500).send(error);
        }
    });
//put
router.put('/', async (req, res) => {
    toSave(req, res, 'classified');
});
//////////delete classified
//get and post
router.get("/delete/:id", async (req, res) => {
    toDelete(req, res, "classifieds", req.params.id);
})
    .post("/delete/:id", async (req, res) => {
        toDelete(req, res, "classifieds", req.params.id);
    })
    //delete
    .delete('/:id', async (req, res) => {
        toDelete(req, res, "classifieds", req.params.id);
    });

//////////update classified
router.post('/update', async (req, res) => {
    try {
        const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/element/get/type/poi/id/${req.body.id}/update/true`,
            { method: 'GET', headers: req.newHeaders });
        ///todo gerer les formats
        const dataCo = await response.json();
        req.body = updateBuilder(req.body, dataCo.map);
        toSave(req, res, "poi");
    } catch (error) {
        res.status(500).send(error);
    }
})
    .patch('/', async (req, res) => {
        try {
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/element/get/type/poi/id/${req.body.id}/update/true`,
                { method: 'GET', headers: req.newHeaders });
            ///todo gerer les formats
            const dataCo = await response.json();
            req.body = updateBuilder(req.body, dataCo.map);
            toSave(req, res, "poi");
        } catch (error) {
            res.status(500).send(error);
        }
    });


router.options('/', function (req, res) { res.sendStatus(405); });



