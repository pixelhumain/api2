
//packages
import express from 'express';
import fetch from 'node-fetch';
import toSave from '../tools/toSave';
//tools
import toDelete from '../tools/toDelete';
import updateBuilder from '../tools/updateBuilder';


const router = express.Router();
export default router;

router.all('/dev', (req, res) => {
    //make your test here
    res.status(200).json(req.body);
})
    ////voir pour le get general si possible
    // router.get("/", async (req, res) => {
    //     try {
    //         const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/project/get`, { method: 'GET', headers: req.newHeaders });
    //         const dataCo = await response.json();
    //         res.status(200).json(dataCo);
    //     } catch (error) {
    //         res.status(500).send(error);
    //     }
    // });
    .get("/:id", async (req, res) => {
        try {
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/element/get/type/poi/id/${req.params.id}`,
                { method: 'GET', headers: req.newHeaders });
            ///todo gerer les formats
            const dataCo = await response.json();
            if (Object.keys(dataCo).length == 0) {
                res.status(200).json({ "result": false, "message": "No data for this element" });
            } else {
                res.status(200).json(dataCo);
            }
        } catch (error) {
            res.status(500).send(error);
        }
    })
    /////////save project
    ///post 
    .post('/', async (req, res) => {
        toSave(req, res, 'poi');
    })
    //put
    .put('/', async (req, res) => {
        toSave(req, res, 'poi');
    })
    //////////delete project
    //get and post
    .get("/delete/:id", async (req, res) => {
        toDelete(req, res, "poi", req.params.id);
    })
    .post("/delete/:id", async (req, res) => {
        toDelete(req, res, "poi", req.params.id);
    })
    //delete
    .delete('/:id', async (req, res) => {
        toDelete(req, res, "poi", req.params.id);
    })

    //////////update project
    .post('/update', async (req, res) => {
        try {
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/element/get/type/poi/id/${req.body.id}/update/true`,
                { method: 'GET', headers: req.newHeaders });
            ///todo gerer les formats
            const dataCo = await response.json();
            req.body = updateBuilder(req.body, dataCo.map);
            toSave(req, res, "poi");
        } catch (error) {
            res.status(500).send(error);
        }
    })
    .patch('/', async (req, res) => {
        toSave(req, res, 'poi');
    });


router.options('/', function (req, res) { res.sendStatus(405); });



