
//packages
import express from 'express';
import fetch from 'node-fetch';
import toSave from '../tools/toSave';
import toDelete from '../tools/toDelete';


const router = express.Router();
export default router;

router.all('/dev', (req, res) => {
    //make your test here
    res.status(200).json(req.body);
})
    .get("/", async (req, res) => {
        try {
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/organization/get`, { method: 'GET', headers: req.newHeaders });
            const dataCo = await response.json();
            res.status(200).json(dataCo);
        } catch (error) {
            res.status(500).send(error);
        }
    })
    .get("/:id", async (req, res) => {
        try {
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/element/get/type/organizations/id/${req.params.id}`, { method: 'GET', headers: req.newHeaders });
            const dataCo = await response.json();
            res.status(200).json(dataCo);
        } catch (error) {
            res.status(500).send(error);
        }
    })
    /////////save 
    .post('/', async (req, res) => {
        toSave(req, res, 'organization');
    })
    //put
    .put('/', async (req, res) => {
        toSave(req, res, 'organization');
    })
    //////////delete 
    //get and post
    .get("/delete/:id", async (req, res) => {
        toDelete(req, res, "organizations", req.params.id);
    })
    .post("/delete/:id", async (req, res) => {
        toDelete(req, res, "organizations", req.params.id);
    })
    //delete
    .delete('/:id', async (req, res) => {
        toDelete(req, res, "organizations", req.params.id);
    })
    //////////update 
    .post('/update', (req, res) => {
        toSave(req, res, "organization");
    })
    .patch('/', (req, res) => {
        toSave(req, res, "organization");
    })
    .options('/', function (req, res) { res.sendStatus(405); });



