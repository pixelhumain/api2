
//packages
import express from 'express';
import fetch from 'node-fetch';
import FormData from 'form-data';


//tools
import toSave from '../tools/toSave';
import toDelete from '../tools/toDelete';
import makeFormKey from '../tools/formDataBuilder';

const router = express.Router();
export default router;

router.all('/dev', (req, res) => {
    //make your test here
    res.status(200).json(req.body);
})
    //special route
    .post('/updatePathValue', async (req, res) => {
        try {
            var toAppend = [];
            const dataToSend = new FormData();

            for (const [k, v] of Object.entries(req.body)) {
                if (typeof v == 'object' || typeof v == 'array') {
                    makeFormKey(k, v, false, toAppend);
                    for (var ks in toAppend) {
                        dataToSend.append(ks, toAppend[ks].toString());
                    }
                } else {
                    dataToSend.append(k, v.toString());
                }
            }
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/element/updatePathValue`,
                { method: 'POST', headers: req.newHeaders, body: dataToSend });
            ///todo gerer les formats
            const dataCo = await response.json();
            res.status(200).send(dataCo);
        } catch (error) {
            res.status(500).send(error);
        }
    })
    .post('/updateSettings', async (req, res) => {
        try {
            var toAppend = [];
            const dataToSend = new FormData();

            for (const [k, v] of Object.entries(req.body)) {
                if (typeof v == 'object' || typeof v == 'array') {
                    makeFormKey(k, v, false, toAppend);
                    for (var ks in toAppend) {
                        dataToSend.append(ks, toAppend[ks].toString());
                    }
                } else {
                    dataToSend.append(k, v.toString());
                }
            }
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/element/updatesettings`,
                { method: 'POST', headers: req.newHeaders, body: dataToSend });
            ///todo gerer les formats
            const dataCo = await response.json();
            res.status(200).send(dataCo);
        } catch (error) {
            res.status(500).send(error);
        }
    })
    .post('/update', async (req, res) => {
        toSave(req, res, req.body.type);
    })
    //get 
    .get("/:type/:id", async (req, res) => {
        try {
            let type = req.params.type;
            if (type == "persons") {
                type = "citoyens";
            }
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/element/get/type/${type}/id/${req.params.id}`, { method: 'GET', headers: req.newHeaders });
            const dataCo = await response.json();
            res.status(200).json(dataCo);
        } catch (error) {
            res.status(500).send(error);
        }
    })
    /////////save element
    ///post 
    .post('/:type', async (req, res) => {
        console.log(req.body)
        toSave(req, res, req.params.type);
    })
    //put
    .put('/:type', async (req, res) => {
        toSave(req, res, req.params.type);
    })
    //////////delete element
    //get and post
    .get("/delete/:type/:id", async (req, res) => {
        toDelete(req, res, req.params.type, req.params.id);
    })
    .post("/delete", async (req, res) => {
        if (typeof req.body.type === 'undefined' || typeof req.body.id === 'undefined') {
            res.status(400).json({ "result": false, "message": "Type and id of the element to delete must be set in body" });
        } else {
            toDelete(req, res, req.body.type, req.body.id);
        }
    })
    //delete
    .delete('/:type/:id', async (req, res) => {
        toDelete(req, res, req.params.type, req.params.id);
    })
    //update
    .patch('/:type/:id', async (req, res) => {
        req.body.id = req.params.id;
        toSave(req, res, req.params.type);
    })

    .options('/', function (req, res) { res.sendStatus(405); });



