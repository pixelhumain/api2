
//packages
import express from 'express';
import fetch from 'node-fetch';

//tools
import toSave from '../tools/toSave';
import toDelete from '../tools/toDelete';
import toUpdate from '../tools/toUpdate';
import updateBuilder from '../tools/updateBuilder';



const router = express.Router();
export default router;

router.all('/dev', (req, res) => {
    //make your test here
    res.status(200).json(req.body);
})
    .get("/:id", async (req, res) => {
        try {
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/element/get/type/classifieds/id/${req.params.id}`, { method: 'GET', headers: req.newHeaders });
            const dataCo = await response.json();
            res.status(200).json(dataCo);
            // const dataCo = await response.text();
            // res.status(200).send(dataCo);
        } catch (error) {
            res.status(500).send(error);
        }
    })
    /////////save project
    ///post 
    .post('/', async (req, res) => {
        toSave(req, res, 'classified');
    })
    //put
    .put('/', async (req, res) => {
        toSave(req, res, 'classified');
    })
    //////////delete classified
    //get and post
    .get("/delete/:id", async (req, res) => {
        toDelete(req, res, "classifieds", req.params.id);
    })
    .post("/delete/:id", async (req, res) => {
        toDelete(req, res, "classifieds", req.params.id);
    })
    //delete
    .delete('/:id', async (req, res) => {
        toDelete(req, res, "classifieds", req.params.id);
    })

    //////////update classified
    .post('/update', async (req, res) => {
        try {
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/element/get/type/classifieds/id/${req.body.id}/update/true`,
                { method: 'GET', headers: req.newHeaders });
            ///todo gerer les formats
            const dataCo = await response.json();
            req.body = updateBuilder(req.body, dataCo.map);
            /// here delete this data else make error in co2
            delete dataCo.map.typeClassified;
            toUpdate(req, res, "classified");
        } catch (error) {
            res.status(500).send(error);
        }
    })
    .patch('/', async (req, res) => {
        try {
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/element/get/type/classifieds/id/${req.body.id}/update/true`,
                { method: 'GET', headers: req.newHeaders });
            ///todo gerer les formats

            const dataCo = await response.json();
            /// here delete this data else make error in co2
            delete dataCo.map.typeClassified;
            req.body = updateBuilder(req.body, dataCo.map);
            toSave(req, res, "classified");
        } catch (error) {
            res.status(500).send(error);
        }
    })


    .options('/', function (req, res) { res.sendStatus(405); });



