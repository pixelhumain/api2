
//packages
import express from 'express';
import fetch from 'node-fetch';
import FormData from 'form-data';
import makeFormKey from '../tools/formDataBuilder';
const router = express.Router();
export default router;

import toDelete from '../tools/toDelete';
import toSave from '../tools/toSave';


//get data 
router.get("/", async (req, res) => {
    try {
        const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/person/get`, { method: 'GET', headers: req.newHeaders });
        const dataCo = await response.json();
        res.status(200).json(dataCo);
    } catch (error) {
        res.status(500).send(error);
    }
})
    .get("/limit/:limit/index/:index", async (req, res) => {
        try {
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/person/get/limit/${req.params.limit}/index/${req.params.index}`,
                { method: 'GET', headers: req.newHeaders });
            const dataCo = await response.json();
            res.status(200).json(dataCo);
        } catch (error) {
            res.status(500).send(error);
        }
    })
    .get("/:id", async (req, res) => {
        try {
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/element/get/type/citoyens/id/${req.params.id}`, { method: 'GET', headers: req.newHeaders });
            const dataCo = await response.json();
            res.status(200).json(dataCo);
        } catch (error) {
            res.status(500).send(error);
        }
    })
    //register
    .post("/register", async (req, res) => {
        try {
            if (typeof req.body == 'undefined') {
                res.status(400).json({ "result": false, "message": "No data send for register" });
            } else {
                const dataTosend = new FormData();
                for (const [k, v] of Object.entries(req.body)) {
                    if (typeof v == 'object' || typeof v == 'array') {
                        makeFormKey(k, v, false, toAppend);
                        for (var ks in toAppend) {
                            dataTosend.append(ks, toAppend[ks].toString());
                        }
                    } else {
                        dataTosend.append(k, v.toString());
                    }
                }
                const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/person/register`, { method: 'POST', headers: req.newHeaders, body: dataTosend });
                try {
                    const dataCo = await response.json();
                    if (dataCo.msg) {
                        dataCo.message = dataCo.msg;
                        delete dataCo.msg;
                    }
                    res.status(200).json(dataCo);
                } catch (error) {
                    const dataCo = await response.text();
                    res.status(400).send("<p>" + dataCo + "</p>");
                }
            }
        } catch (error) {
            res.status(500).send(error);
        }
    })
    .put('/', async function (req, res) {
        if (typeof req.body == 'undefined') {
            res.status(400).json({ "result": false, "message": "No data send for register" });
        } else {
            try {
                const dataTosend = new FormData();
                for (const [k, v] of Object.entries(req.body)) {
                    if (typeof v == 'object' || typeof v == 'array') {
                        makeFormKey(k, v, false, toAppend);
                        for (var ks in toAppend) {
                            dataTosend.append(ks, toAppend[ks]);
                        }
                    } else {
                        dataTosend.append(k, v);
                    }
                }
                const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/person/register`, { method: 'POST', headers: req.newHeaders, body: dataTosend });
                const dataCo = await response.json();
                if (dataCo.msg) {
                    dataCo.message = dataCo.msg;
                    delete dataCo.msg;
                }
                res.status(200).json(dataCo);
            } catch (error) {
                res.status(500).send(error);
            }
        }
    })
    //login and logout
    .post("/login", async (req, res) => {
        if (typeof req.body.email == 'undefined' || req.body.email == "") {
            res.status(400).json({ "result": false, "message": "Email is required for login" });
        } else if (typeof req.body.pwd == 'undefined' || req.body.pwd == "") {
            res.status(400).json({ "result": false, "message": "Password is required for login" });
        } else {
            try {
                const dataTosend = new FormData();
                dataTosend.append('email', req.body.email);
                dataTosend.append('pwd', req.body.pwd);
                const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/person/authenticate`, { method: 'POST', headers: req.newHeaders, body: dataTosend });
                const dataCo = await response.json();
                if (dataCo.msg) {
                    dataCo.message = dataCo.msg;
                    delete dataCo.msg;
                }
                res.status(200).json(dataCo);
                //res.status(200).json(dataCo);
            } catch (error) {
                res.status(500).send(error);
            }
        }
    })
    .get("/logout", async (req, res) => {
        try {
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/person/logout`, { method: 'GET', headers: req.newHeaders });
            const dataCo = await response.json();
            res.status(200).json(response);
        } catch (error) {
            res.status(500).send(error);
        }
    })
    .delete('/:id', function (req, res) {
        toDelete(req, res, "citoyens", req.params.id);
    })
    .get('/delete', async function (req, res) {
        toDelete(req, res, "citoyens", req.query.id);
    })
    .post('/delete', async function (req, res) {
        toDelete(req, res, "citoyens", req.body.id);
    })
    //update
    .patch('/field', async function (req, res) {
        try {
            var toAppend = [];
            const dataToSend = new FormData();

            for (const [k, v] of Object.entries(req.body)) {
                if (typeof v == 'object' || typeof v == 'array') {
                    makeFormKey(k, v, false, toAppend);
                    for (var ks in toAppend) {
                        dataToSend.append(ks, toAppend[ks]);
                    }
                } else {
                    dataToSend.append(k, v);
                }
            }
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/person/updateField`,
                { method: 'POST', headers: req.newHeaders, body: dataToSend });
            ///todo gerer les formats
            // const dataCo = await response.json();
            const dataCo = await response.text();
            res.status(200).send(dataCo);
        } catch (error) {
            res.status(500).send(error);
        }
    })
    .patch('/', async function (req, res) {
        toSave(req, res, "persons")
    })
    .options('/', function (req, res) { res.sendStatus(405); });




