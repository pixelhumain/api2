
//packages
import express from 'express';
import fetch from 'node-fetch';
import toSave from '../tools/toSave';
import toUpdate from '../tools/toUpdate';
import toDelete from '../tools/toDelete';


const router = express.Router();
export default router;

router.all('/dev', (req, res) => {
    //make your test here
    res.status(200).json(req.body);
});
// route to get news for element, all post, three possibility of parameters schema
router.post("/get", async (req, res) => {
    try {
        const response = await fetch(`${process.env.HOST_TO_FETCH}/news/co/get/type/${req.body.type}/id/${req.body.id}/isLive/${req.body.isLive}/date/${req.body.date}`, { method: 'POST', headers: req.newHeaders });
            const dataText = await response.text();
            res.status(200).json(dataText);
        } catch (error) {
            res.status(500).json(error);
    }
})
.post("/get/:type/:id", async (req, res) => {
    try {
        const response = await fetch(`${process.env.HOST_TO_FETCH}/news/co/get/type/${req.params.type}/id/${req.params.id}/isLive/${req.body.isLive}/date/${req.body.date}`, { method: 'POST', headers: req.newHeaders });
            const dataText = await response.text();
            res.status(200).json(dataText);
        } catch (error) {
            res.status(500).json(error);
    }
})
.post("/get/type/:type/id/:id/isLive/:bool/date/:date", async (req, res) => {
    try {
        const response = await fetch(`${process.env.HOST_TO_FETCH}/news/co/get/type/${req.params.type}/id/${req.params.id}/isLive/${req.params.bool}/date/${req.params.date}`, { method: 'POST', headers: req.newHeaders });
            const dataText = await response.text();
            res.status(200).json(dataText);
        } catch (error) {
            res.status(500).json(error);
    }
});
//route get index
router.post("/index", async (req, res) => {
    try {
        const response = await fetch(`${process.env.HOST_TO_FETCH}/news/co/index/type/${req.body.type}/id/${req.body.id}/isLive/${req.body.isLive}/date/${req.body.date}`, { method: 'POST', headers: req.newHeaders });
            const dataText = await response.text();
            res.status(200).json(dataText);
        } catch (error) {
            res.status(500).json(error);
    }
})
.post("/index/:type/:id", async (req, res) => {
    try {
        const response = await fetch(`${process.env.HOST_TO_FETCH}/news/co/index/type/${req.params.type}/id/${req.params.id}/isLive/${req.body.isLive}/date/${req.body.date}`, { method: 'POST', headers: req.newHeaders });
            const dataText = await response.text();
            res.set('Content-Type', 'text/html');
            res.status(200).json(dataText);
        } catch (error) {
            res.status(500).json(error);
    }
})
.post("/index/type/:type/id/:id/isLive/:bool", async (req, res) => {
    try {
        const response = await fetch(`${process.env.HOST_TO_FETCH}/news/co/index/type/${req.params.type}/id/${req.params.id}/isLive/${req.params.bool}/date/${req.params.date}`, { method: 'POST', headers: req.newHeaders });
            const dataText = await response.text();
            res.status(200).json(dataText);
        } catch (error) {
            res.status(500).json(error);
    }
});
// router.get("/:id", async (req, res) => {
//     try {
//         const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/project/get/id/${req.params.id}`, { method: 'GET', headers: req.newHeaders });
//         const dataCo = await response.json();
//         res.status(200).json(dataCo);
//     } catch (error) {
//         res.status(500).send(error);
//     }
// });
// /////////save project
// ///post 
// router.post('/', async (req, res) => {
//     toSave(req, res, 'project');
// });
// //put
// router.put('/', async (req, res) => {
//     toSave(req, res, 'project');
// });
// //////////delete project
// //get and post
// router.get("/delete/:id", async (req, res) => {
//     toDelete(req, res, "projects", req.params.id);
// })
// .post("/delete/:id", async (req, res) => {
//     toDelete(req, res, "projects", req.params.id);
// })
// //delete
// .delete('/:id', async (req, res) => {
//     toDelete(req, res, "projects", req.params.id);
// });

// //////////update project
// router.post('/update', (req, res) => { 
//     toUpdate(req, res, "project");
// })
// .patch('/', (req, res) => { 
//     toUpdate(req, res, "project");
// });


router.options('/', function (req, res) { res.sendStatus(405); });



