//packages
import fetch from 'node-fetch';
import express from 'express';
import FormData from 'form-data';
//db
import db from "../models";
const router = express.Router();
require('dotenv').config();


// define the call route
router.use(function timeLog(req, res, next) {
    res.set('Content-Type', 'application/json');
    next();
})

    .post('/', async function (req, res) {
        ///check params
        if (typeof req.body.email == "undefined" || typeof req.body.pwd == "undefined") {
            res.status(400)
                .send({ message: "You must send email and pwd in body to authenticate!" })
        } else if (req.body.email == "" || req.body.pwd == "") {
            res.status(400)
                .send({ message: "Email and pwd cannot be empty!" })
        } else {
            ////////make body and fetch co token (TODO vérife and gestion erreurs)
            const form = new FormData();
            form.append('email', req.body.email);
            form.append('pwd', req.body.pwd);
            form.append('tokenName', 'api2');
            try {
                const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/person/authenticatetoken`, { method: 'POST', body: form });
                const json = await response.json();
                if (json.result) {
                    //mongo connect and save
                    db.mongoose
                        .connect(db.url, {
                            useNewUrlParser: true,
                            useUnifiedTopology: true,
                            useFindAndModify: false
                        })
                        .catch((err) => {
                            //res.status(500).json({message: err}).end();
                            console.log(err);
                        })
                    const users = require("../controllers/user.controller.js");
                    users.create(req, res, json);

                } else {
                    res.status(400).send(json)
                }
            } catch (error) {
                res.status(503).json({ status: false, message: "Error from authorization server", error: error });
            }
        }
    })
    //only post is allowed else return 405 response
    .get('/', function (req, res) { res.sendStatus(405); })
    .put('/', function (req, res) { res.sendStatus(405); })
    .delete('/', function (req, res) { res.sendStatus(405); })
    .patch('/', function (req, res) { res.sendStatus(405); })
    .options('/', function (req, res) { res.sendStatus(405); });

module.exports = router;

