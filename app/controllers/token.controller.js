import db from "../models";
import jwt from "jsonwebtoken";

require('dotenv').config();

const User = db.user;

// Create and Save a new user
exports.checkToken = async (req, res, next) => {
    db.mongoose
        .connect(db.url, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        .catch((err) => { console.log(err) })
    try {
        const token = req.header('Authorization').replace('Bearer ', '');
        const data = jwt.verify(token, process.env.JWT_SECRET);
        const user = await User.findOne({ id: data.id });
        if (!user) {
            res.status(498).send({ result: false, message: 'Bad token' });
        } else {
            req.newHeaders = {
                'X-Auth-Token': user.token,
                'X-User-Id': user.id,
                'X-Auth-Name': 'api2',
            };
            next();
        }
    } catch (error) {
        res.status(498).json({ status: false, message: "Token is required for this api." });
    }
    db.mongoose.connection.close()
};