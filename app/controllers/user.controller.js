import db from "../models/index";
import jwt from "jsonwebtoken";

require('dotenv').config();

const User = db.user;

// Create and Save a new user
exports.create = async (req, res, json) => {
    //check if user exist in db 
    await User.findOne({ id: json.id }, function (err, user) {
        if (user) {
            //////////voir ici pour le recreate token
            res.status(200).send({ message: 'User already register' });
        } else {
            const accessToken = jwt.sign({ id: json.id }, process.env.JWT_SECRET);
            const user = new User({
                id: json.id,
                token: json.token
            });
            user.save((err) => {
                    if (err) {
                        res.status(500).send({ message: err });
                        return;
                    }

                    res.status(201).send({ result: true, accessToken: accessToken });
                });
        }
    });
};

// Retrieve all users from the database.
exports.findAll = (req, res) => {

};

// Find a single user with an id
exports.findOne = (req, res) => {

};

// Update a user by the id in the request
exports.update = (req, res) => {

};

// Delete a user with the specified id in the request
exports.delete = (req, res) => {

};

// Delete all users from the database.
exports.deleteAll = (req, res) => {

};

// Find all published users
exports.findAllPublished = (req, res) => {

};
