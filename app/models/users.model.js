import mongoose from "mongoose";

const User = mongoose.model(
  "User",
  new mongoose.Schema({
    id: String,
    token: String,
  })
);

module.exports = User;

