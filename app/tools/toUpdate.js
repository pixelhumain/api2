import fetch from 'node-fetch';
import FormData from 'form-data';
import toSave from './toSave';
import updateBuilder from './updateBuilder';


export default async function toUpdate(req, res, element) {
    try {
        const dataTosend = new FormData();
        dataTosend.append('key', element);
        if (element === 'poi') {
            var collection = element;
        } else {
            var collection = element + "s";
        }
        const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/element/get/type/${collection}/id/${req.body.id}/update/true`,
            { method: 'GET', headers: req.newHeaders });
        const dataCo = await response.json();
        req.body = updateBuilder(req.body, dataCo.map);
        //console.log(dataCo);
        console.log("test======-------------------")
        toSave(req, res, element);
    } catch (err) {
        res.status(500).send(err);
    }

}