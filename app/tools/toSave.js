import fetch from 'node-fetch';
import FormData from 'form-data';
import makeFormKey from './formDataBuilder';

export default async function toSave(req, res, element) {
    try {
        const dataTosend = new FormData();
        dataTosend.append('key', element);
        if (element === 'poi') {
            dataTosend.append('collection', element);
        } else {
            dataTosend.append('collection', element + "s");
        }
        //console.log(dataTosend)
        var toAppend = [];

        for (const [k, v] of Object.entries(req.body)) {
            if (typeof v == 'object' || typeof v == 'array') {
                makeFormKey(k, v, false, toAppend);
                for (var ks in toAppend) {
                    dataTosend.append(ks, toAppend[ks].toString());
                }
            } else {
                dataTosend.append(k, v.toString());
            }
        }
        //console.log(dataTosend)
        const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/element/save`, { method: 'POST', headers: req.newHeaders, body: dataTosend });
        const dataCo = await response.json();
        /////////TODO message en fonction dataCo
        //const dataCo = await response.text();
        //console.log(dataCo);
        // res.status(200).send(dataCo);
        res.status(200).send(dataCo);
    } catch (error) {
        res.status(500).send(error);
    }

}