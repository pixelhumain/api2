import fetch from 'node-fetch';
import db from "../models";
import FormData from 'form-data';
import makeFormKey from './formDataBuilder';


export default async function (req, res, type, id) {
    try {
        if (typeof req.body.reason === 'undefined') {
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/element/delete/type/${type}/id/${id}`, { method: 'GET', headers: req.newHeaders });
            var dataCo = await response.json();
        } else {
            const dataTosend = new FormData();
            var toAppend = [];
            for (const [k, v] of Object.entries(req.body)) {
                if (typeof v == 'object' || typeof v == 'array') {
                    makeFormKey(k, v, false, toAppend);
                    for (var ks in toAppend) {
                        dataTosend.append(ks, toAppend[ks]);
                    }
                } else {
                    dataTosend.append(k, v);
                }
            }
            const response = await fetch(`${process.env.HOST_TO_FETCH}/co2/element/delete/type/${type}/id/${id}`, { method: 'POST', headers: req.newHeaders, body: dataTosend });
            var dataCo = await response.json();
        }
        if (dataCo.msg) {
            dataCo.message = dataCo.msg;
            delete dataCo.msg;
        }
        if (dataCo.result) {
            if (type === "citoyens") {
                db.mongoose
                    .connect(db.url, {
                        useNewUrlParser: true,
                        useUnifiedTopology: true,
                        useFindAndModify: false
                    })
                    .catch((err) => { console.log(err) });
                const User = db.user;
                const user = await User.findOneAndRemove({ id: id }, function (err, docs) {
                    if (err) {
                        console.log(err)
                    }
                    else {
                        console.log("Result : ", dataCo);
                    }
                });
            }
            res.status(200).json(dataCo);
        } else {
            res.status(400).json(dataCo);
        }
        db.mongoose.connection.close();
    } catch (err) {
        db.mongoose.connection.close();
        res.status(500).json({ "test": err });
    }
};