export default function UpdateBuilder(body, dataOfPoi) {
    let updateObj = Object.assign(dataOfPoi, body);
    delete updateObj._id;
    if (body.parent) {
        updateObj.parent = body.parent;
    } else {
        delete updateObj.parent;
    }
    delete updateObj.modified;
    delete updateObj.updated;
    delete updateObj.created;
    delete updateObj.creator;
    delete updateObj.typeSig;
    delete updateObj.counts;
    delete updateObj.slug;
    delete updateObj.type;
    return updateObj;
}