
export default function makeFormKey(key, object, stop, ret = []) {
    if (stop) {
        return ret;
    }
    for (const [k, v] of Object.entries(object)) {
            var newKey = key + "[" + k + "]";
        if (typeof v == 'object' || typeof v == 'array') {
            makeFormKey(newKey, v, false, ret);
        } else {
            ret[newKey] = v;
        }
    }
    return makeFormKey(null, null, true, ret);
}