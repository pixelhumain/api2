# Api 2

## Introduction

   This project is an independent server written in nodejs, which adds an authentication layer using jsonwebtoken and forms an api for communecter. The authorisation is still done in the communecter core. 
   A small mongodb database for authentication data is also planned.
    
## Installation

   This project requires the installation of nodejs (v10.21.0 used for development) and mongodb.
   Clone the repository.
   At the root of the project type in the terminal to install the dependencies:in the files
```
   npm i

```
Then always in the terminal in order to have nodemon for the script and the automatic restart of the server after safeguard in the files (you can put it in general it is well practical with the options -g)
```
   npm i nodemon

```
   an environment file .env is to be created at the root of the project with a secret entered the listening port of your api and the host communecter on which you work:

```
   JWT_SECRET =<YourSecret>
   PORT_SERVER=<YourServerPort>
   HOST_TO_FETCH=<communecterHOST>or<LocalHOSTforDEV>

```
   the config mongodb file is /app/config/db.config.js with :
```
const HOST = "yourHost";
const PORT = "yourPort";
const DB = "yourDbName";
module.exports = {
    url: `mongodb://${HOST}:${PORT}/${DB}`
  };
```
   Once done and properly configured you can run the startup script:
```
npm start

```

## Test

   Some tests allow to check the functioning of a part of the routes

   First of all you need a token of the api.
   Use postman or curl to get it with your communecter ID (example curl):

```
curl --location --request POST 'http://<YourHost>:<YourServerPort>/api2/call' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "<communecterEmail>",
    "pwd": "communecterPassword"
}'
```
__Attention__ keep well the returned token for all your tests and your calls to the api if you retry the operation no token will be returned to you 
This part is not yet well managed, and to obtain a new one it will be necessary for you to edit the user in db on co by removing the part loginToken that whose name is "api2" and
delete the user in the db of the api then start again the operation.
 (or delete the user on co via the api in this case the user on the api db is automatically destroyed)

 The uri /api2/call is the only route accessible without token.

 Then you need to register a test user via (example curl):
 ```
 curl --location --request POST 'http://<YourHost>:<YourServerPort>/api2/persons/register' \
--header 'Authorization: Bearer <Le token que vous avez récupérez>' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "user test postman",
    "username": "test postman",
    "email": "postman@yahoo.fr",
    "pwd": "plopplop",
    "app": "co2",
    "mode": "two_steps_register"
}'
```
You need to validate the user's email for the docker version of co type in the console at the docker level:
```
docker-compose -f docker-compose.yml -f docker-compose.install.yml run ph cotools --emailvalid=postman@yahoo.fr
```
All this done (an automation of these procedures would be a good contribution from coders willing to participate)
you can now launch the tests via (at the root of the api2 repository):

```
newman run API2.postman_collection.json 
```
In the event of a test failure, check that there is no data left in the co db and the user test in the api db.

### Tests done

![Liste des tests effectués](./images/testApi2_01.png "Liste des test effectués")
![Liste des tests effectués](./images/testApi2_02.png)


## HTTPS

Https can be enabled if you have certificates create a certificate folder at the root of the project and put the certificate and key in it
such as :

/certificat/server.cert

/certificat/server.key

then in server.js at the begining uncomment:
```
...

//cert for https 
 const key = fs.readFileSync(path.join(__dirname, 'certificat', 'server.key'));
 const cert = fs.readFileSync(path.join(__dirname, 'certificat', 'server.cert'));
 const options = { key, cert };

...

//https
 https.createServer(options, app).listen(process.env.PORT_SERVER, () => {
   console.log(`Server is running on port ${process.env.PORT_SERVER}.`);
 });
```
and comment  http:

```
//http
// app.listen(process.env.PORT_SERVER, () => {
//  console.log(`Server is running on port ${process.env.PORT_SERVER}.`);
// });
```
