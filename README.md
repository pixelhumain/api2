# Api 2

## Introduction

   Ce projet est un server indépendant écrit en nodejs, qui ajoute une couche d'authentification a l'aide de jsonwebtoken et forme une api pour communecter. L'authorisation elle se fait toujours dans le coeur de communecter. 
   Une petite base de données mongodb pour les données d'authentification et a prévoir également.
    
## Installation

   Ce projet nécessite l'installation de nodejs (v10.21.0 utilisé pour le dévellopement) ainsi que mongodb.
   Cloner le dépot.
   A la racine du projet  taper dans le terminal  pour installer les dependances:dans les fichier
```
   npm i

```
Puis toujours dans le terminal  afin d'avoir nodemon pour les script et le redémarage automatique du server aprés sauvegarde dans les fichier (vous pouvez le mettre en general c'est bien pratique avec l'options -g)
```
   npm i nodemon

```
   un fichier d'environnement  .env est a créer à la racine du projet avec un secret a rentré le port d'écoute de votre api et l'host communecter sur lequelle vous travaillez:

```
   JWT_SECRET =<YourSecret>
   PORT_SERVER=<YourServerPort>
   HOST_TO_FETCH=<communecterHOST>or<LocalHOSTforDEV>

```
   le fichier de config mongodb est a créer /app/config/db.config.js avec :
```
const HOST = "yourHost";
const PORT = "yourPort";
const DB = "yourDbName";
module.exports = {
    url: `mongodb://${HOST}:${PORT}/${DB}`
  };
```
   Une fois fait et bien configurer vous pouvez lancer le script de démarage:
```
npm start

```

## Test

   Des test permettent de vérifier le fonctionnement d'une partie des routes

   Tout d'abord il vous faut un token de l'api.
   Utilisez postman ou curl afin de l'obtenir avec vos identifiant communecter (exemple curl):

```
curl --location --request POST 'http://<YourHost>:<YourServerPort>/api2/call' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "<communecterEmail>",
    "pwd": "communecterPassword"
}'
```
__Attention__ garder bien le token renvoyé pour tout vos test et vos appels a l'api si vous retentez l'opération aucun token ne vous seras renvoyé 
Cette partie n'est pas encore bien gérer, et pour obtenir un nouveau il vous faudras editer l'user en db sur co en retirant la partie loginToken celle dont le nom est "api2" et
delete l'user dans la db de l'api puis recommencer l'opération.
 (ou supprimé l'user sur co via l'api dans ce cas l'user sur la db de l'api est detruit automatiquement)

 l'uri /api2/call est la seul route accessible sans token.

 Ensuite il vous faut enregistré un utilisateur test via (exemple curl):
 ```
 curl --location --request POST 'http://<YourHost>:<YourServerPort>/api2/persons/register' \
--header 'Authorization: Bearer <Le token que vous avez récupérez>' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "user test postman",
    "username": "test postman",
    "email": "postman@yahoo.fr",
    "pwd": "plopplop",
    "app": "co2",
    "mode": "two_steps_register"
}'
```
Il faut rentrer en dur la value de l'id de l'utilisateur qui vous a été renvoyer par l'appel curl de création de l'utilisateur test dans le fichier /api2/API2.postman_collection.json ligne 2244
```
{
			"id": "ff733d39-cf69-46d8-8410-986b981e489a",
			"key": "id user test",
			"value": "<id renvoyer par la création de l'utilisateur test>"
		},
```

La il vous faut valider le mail de l'utilisateur pour la version docker de co taper dans la console au niveau du docker:
```
docker-compose -f docker-compose.yml -f docker-compose.install.yml run ph cotools --emailvalid=postman@yahoo.fr
```

Tout ceci fait (une automatisation de ces procédures serait un bon apport de la part des codeurs voulant participer)
vous pouvez maintenant lancer les tests via (a la racine du dépot de l'api2):

```
newman run API2.postman_collection.json 
```
__Attention__ en cas d'echec de tests vérifier qu'il ne reste pas des données en db de co et l'user test en db de l'api.

### Liste des tests effectués

![Liste des tests effectués](./images/testApi2_01.png "Liste des test effectués")
![Liste des tests effectués](./images/testApi2_02.png)


## HTTPS

L'https peut étre activé si vous avez des certificats créer un dossier certificat a la racine du projet et mettez dedans le certificat et la clé
tel que :

/certificat/server.cert

/certificat/server.key

Puis dans server.js a la racine décommentez:
```
...

//cert for https 
 const key = fs.readFileSync(path.join(__dirname, 'certificat', 'server.key'));
 const cert = fs.readFileSync(path.join(__dirname, 'certificat', 'server.cert'));
 const options = { key, cert };

...

//https
 https.createServer(options, app).listen(process.env.PORT_SERVER, () => {
   console.log(`Server is running on port ${process.env.PORT_SERVER}.`);
 });
```
et commentez le http:

```
//http
// app.listen(process.env.PORT_SERVER, () => {
//  console.log(`Server is running on port ${process.env.PORT_SERVER}.`);
// });
```
