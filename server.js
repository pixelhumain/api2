//nodejs package
import fs from 'fs';
import path from 'path';
import https from 'https';
import express from "express";
import bodyParser from "body-parser";
import cors from 'cors';
//router
import call from './app/routers/call.router';
import persons from './app/routers/persons.router';
import projects from './app/routers/projects.router';
import events from './app/routers/events.router';
import orgas from './app/routers/orgas.router';
import pois from './app/routers/pois.router';
import classifieds from './app/routers/classifieds.router';
import element from './app/routers/element.router';
import link from './app/routers/link.router';
import news from './app/routers/news.router';
//token 
import check from './app/controllers/token.controller.js';


const app = express();
//cert for https 
// const key = fs.readFileSync(path.join(__dirname, 'certificat', 'server.key'));
// const cert = fs.readFileSync(path.join(__dirname, 'certificat', 'server.cert'));
// const options = { key, cert };
////////////cors
app.use(cors({
  allowedHeaders: ['Content-Type', 'Authorization'],
  origin: '*',
  methods: 'GET,PUT,PATCH,POST,DELETE',
}))
  // parse requests of content-type - application/json
  .use(bodyParser.json())
  // parse requests of content-type - application/x-www-form-urlencoded
  .use(bodyParser.urlencoded({ extended: true }))
  // call route to get token 
  .use("/api2/call", call)
  .use(async function (req, res, next) {
    await check.checkToken(req, res, next)
  })

  //collections route
  .use("/api2/element", element)
  .use("/api2/persons", persons) ///TODO voir pour register sans le token puis le renvoyer
  .use("/api2/projects", projects)
  .use("/api2/organizations", orgas)
  .use("/api2/pois", pois)
  .use("/api2/classifieds", classifieds)
  .use("/api2/events", events)
  .use("/api2/link", link)
  .use("/api2/news", news);

//ctach all other route with 404 response
app.use(function (req, res, next) {
  res.setHeader('Content-Type', 'application/json');
  res.status(404).json({ result: false, Message: 'No route for this url' });
});
//set port, listen for requests
//https
// https.createServer(options, app).listen(process.env.PORT_SERVER, () => {
//   console.log(`Server is running on port ${process.env.PORT_SERVER}.`);
// });
//http
app.listen(process.env.PORT_SERVER, () => {
  console.log(`Server is running on port ${process.env.PORT_SERVER}.`);
});
